.PHONY: tests lynt
tests:
	python -m pytest tests -v -W ignore
lynt:
	python -m pylint .
install:
	pip install -r requirements.txt
