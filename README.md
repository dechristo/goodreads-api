# Goodreads API XML to JSON converter  [![Build Status](https://travis-ci.org/dudu84/goodreads-api-json.svg?branch=master)](https://travis-ci.org/dudu84/goodreads-api-json)

Converts Goodreads API response (xml format) to JSON.
Implemented in Python 3 with Flask.

