from src.extractor.strategy import Strategy
from xml.etree import ElementTree


class AuthorStrategy(Strategy):

    def extract(self, text):
        xml = ElementTree.fromstring(text)
        author_data = xml.find('author')
        authors_books = author_data.find('books')
        books = []
        for b in authors_books:
            book = {
                'title': b.find('title').text,
                'image_url': b.find('image_url').text,
                'pages' : b.find('pages'),
                'format' : b.find('format'),
                'year' : b.find('publication_year'),
                'description' : b.find('description')
            }
            books.append(book)
        author = {
            'name': author_data.find('name').text,
            'link': author_data.find('link').text,
            'about': author_data.find('about').text,
            'hometown': author_data.find('hometown').text,
            'books': books
        }
        return author
