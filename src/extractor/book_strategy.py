from src.extractor.strategy import Strategy
from xml.etree import ElementTree


class BookStrategy(Strategy):

    def extract(self, text):
        xml = ElementTree.fromstring(text)
        search = xml.find('search')
        books = []
        works = search.find('results').findall('work')
        for work in works:
            found_book = work.find('best_book')
            avg_rating = float(work.find('average_rating').text)
            year = ''
            if work.find('original_publication_year').text:
                year = int(work.find('original_publication_year').text)
            book = {
                'title': found_book.find('title').text,
                'author': found_book.find('author').find('name').text,
                'year': year,
                'average_rating': avg_rating,
                'image_url': found_book.find('image_url').text
            }
            books.append(book)
        return books
