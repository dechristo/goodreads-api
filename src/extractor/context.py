class Context:

    def __init__(self, strategy):
        self.__strategy = strategy

    def extract(self, text):
        result = self.__strategy.extract(text)
        return result
