from abc import ABC, abstractmethod


class Strategy(ABC):

    @abstractmethod
    def extract(self, text):
        pass
