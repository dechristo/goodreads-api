from flask import Flask, jsonify, request
from settings import Settings
import requests
from src.utils.x2j_converter import X2JConverter

APP = Flask(__name__)

@APP.route("/api")
def home():
    content = {"msg": "Hey there! Welcome to good reads api data extractor!"}
    return __send_response(content, 200)

@APP.route("/api/book", methods=["GET"])
def search_book():
    title = request.args.get('title')
    if not title:
        return __send_response({"error":"empty title"}, 400)
    url = Settings.GOODREADS_BASE_URL + '/search?key={0}&q={1}'.format('8B8atVv62bvF0ldjR0NwGQ',title)
    response = requests.get(url, stream=True)
    result = X2JConverter.from_string(response.text, 'book')
    return __send_response(result, 200)

@APP.route("/api/author", methods=["GET"])
def search_author():
    name = request.args.get('name')
    if not name:
        return __send_response({"error":"empty name"}, 400)
    url = Settings.GOODREADS_BASE_URL + '/search?key={0}&q={1}'.format('8B8atVv62bvF0ldjR0NwGQ',name)
    response = requests.get(url, stream=True)
    result = X2JConverter.from_string(response.text, 'author')
    return __send_response(result, 200)

def __send_response(content, status_code):
    return jsonify({'data':content}), status_code
