from src.extractor.book_strategy import BookStrategy
from src.extractor.author_strategy import AuthorStrategy
from src.extractor.context import Context


class X2JConverter:

    @classmethod
    def from_string(cls, text, strategy='book'):
        if not cls.__is_input_valid(text):
            return {}
        concrete_strategy = cls.__getConcreteStrategy(strategy)
        result = cls.__extract(text, concrete_strategy)
        return result

    @classmethod
    def __extract(cls, text, concrete_strategy):
        """pass the strategy as a parameter in the method"""
        context = Context(concrete_strategy)
        result = context.extract(text)
        return result

    @classmethod
    def __is_input_valid(cls, param):
        if not param:
            return False
        if not isinstance(param, str):
            raise TypeError('Invalid input parameter type. Must be a string')
        return True

    @classmethod
    def __getConcreteStrategy(cls, strategy):
        if strategy == 'book':
            return BookStrategy()
        if strategy == 'author':
            return AuthorStrategy()
