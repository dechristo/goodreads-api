import json
from src.router import  APP
import unittest


class RouterTest(unittest.TestCase):

    def setUp(self):
        APP.testing = True
        self.app = APP.test_client()

    def test_default_router_returns_correct_msg_and_http_200(self):
        response = self.app.get('http://localhost:5000/api')
        obj = json.loads(response.data.decode('utf-8'))
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type ,'application/json')
        self.assertIn('data', obj)
        self.assertIn('msg', obj['data'])
        self.assertEqual(obj['data']['msg'],"Hey there! Welcome to good reads api data extractor!")

    def test_search_book_by_keyword_with_GET_returns_json(self):
        url = '/api/book?title={0}'.format('storm john sandford')
        response = self.app.get(url)
        obj = json.loads(response.data.decode('utf-8'))
        self.assertIsNotNone(obj)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

    def test_search_book_with_empty_keyword_and_GET_returns_http_404(self):
        url = '/api/book?title='.format('')
        response = self.app.get(url)
        self.assertEqual(response.status_code, 400)

    def test_search_book_with_keyword_and_POST_returns_http_405(self):
        url = '/api/book?title={0}'.format('altar of eden')
        response = self.app.post(url)
        self.assertEqual(response.status_code, 405)

    def test_search_book_with_keyword_and_PUT_returns_http_405(self):
        url = '/api/book?title={0}'.format('altar of eden')
        response = self.app.put(url)
        self.assertEqual(response.status_code, 405)

    def test_search_book_with_keyword_and_DELETE_returns_http_405(self):
        url = '/api/book?title={0}'.format('altar of eden')
        response = self.app.delete(url)
        self.assertEqual(response.status_code, 405)
