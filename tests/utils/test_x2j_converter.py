import unittest
from src.utils.x2j_converter import X2JConverter


class X2JConverterTest(unittest.TestCase):

    def setUp(self):
        self.xml_mock = """<root>
            <search>
                <results>
                    <work>
                        <average_rating>4.3</average_rating>
                        <original_publication_year>2028</original_publication_year>
                        <best_book>
                            <title>The Gale</title>
                            <author>
                                <name>Luiz Eduardo de Christo</name>
                            </author>
                            <year>2028</year>
                            <image_url>https://goodreads.com/images/urlyettocome.jpg</image_url>
                        </best_book>
                    </work>
                </results>
            </search>
        </root>"""

    def test_returns_empty_object_if_from_string_method_parameter_is_empty(self):
        result = X2JConverter.from_string('')
        self.assertEqual(result, {})

    def test_raises_type_error_exception_if_from_string_input_param_is_not_a_string(self):
        self.assertRaises(TypeError, X2JConverter.from_string, 2)
        self.assertRaises(TypeError, X2JConverter.from_string, 56.98)
        self.assertRaises(TypeError, X2JConverter.from_string, True)
        self.assertRaises(TypeError, X2JConverter.from_string, [1, 2, 3])
        self.assertRaises(TypeError, X2JConverter.from_string, {0 : "Testing..."})
        self.assertRaises(TypeError, X2JConverter.from_string, (1,2))

    def test_returns_correct_json_for_valid_xml(self):
        result = X2JConverter.from_string(self.xml_mock)
        self.assertIsNotNone(result)
        self.assertIsInstance(result, list)
        self.assertIn('title', result[0])
        self.assertEqual('The Gale', result[0]['title'])
        self.assertIn('author', result[0])
        self.assertEqual('Luiz Eduardo de Christo', result[0]['author'])
        self.assertIn('year', result[0])
        self.assertEqual(2028, result[0]['year'])
        self.assertIn('average_rating', result[0])
        self.assertEqual(4.3, result[0]['average_rating'])
        self.assertIn('image_url', result[0])
        self.assertEqual('https://goodreads.com/images/urlyettocome.jpg', result[0]['image_url'])
